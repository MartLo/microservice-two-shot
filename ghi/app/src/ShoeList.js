function ShoeList(props) {
    return (
        <table className='table table-striped'>
            <thead>
                <tr>
                    <th>Shoe List</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes_rest && props.shoes_rest.map(shoes => {
                    return (
                        <tr key={shoes_rest.href}>
                            <td>{shoes_rest}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}

export default ShoeList;
