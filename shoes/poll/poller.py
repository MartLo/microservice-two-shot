import django
import os
import sys
import time
import json
import requests
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from shoes_rest.models import Shoe


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "shoes_project.settings")
django.setup()

# Import models from hats_rest, here.
# from shoes_rest.models import Something


def shoe_poll():
    wardrobe_api = "http://wardrobe-api:8000/api/bins/"
    headers = {
        "Authorization": f"Bearer {settings.SHOES_REST_TOKEN}"
    }

    try:
        response = requests.get(wardrobe_api, headers=headers)
        response.raise_for_status()
        data = response.json()

        for item in data:
            manufacturer = item["manufacturer"]
            model_name = item["model_name"]
            color = item["color"]
            picture_url = item["picture_url"]
            bin = item["bin"]

            try:
                shoes = Shoe.objects.get(manufacturer=manufacturer, model_name=model_name)
                shoes.color = color
                shoes.picture_url = picture_url
                shoes.bin = bin
                shoes.save()
            except ObjectDoesNotExist:
                Shoe.objects.create(manufacturer=manufacturer, model_name=model_name,
                                    color=color, picture_url=picture_url, bin=bin)

        print("Polling and updating Shoe data completed successfully.")

    except requests.exceptions.RequestException as e:
        print(f"Error while polling Wardrobe API: {e}")


if __name__ == "__main__":
    shoe_poll()


def poll():
    while True:
        print('Shoes poller polling for data')
        try:
            # Write your polling logic, here
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
