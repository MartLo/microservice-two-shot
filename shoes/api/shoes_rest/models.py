from django.db import models


class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=200)


class Shoe(models.Model):
    manufacturer = models.CharField(max_length=100)
    model_name = models.CharField(max_length=100, default='')
    color = models.CharField(max_length=50)
    picture_url = models.URLField(default='')
    bin = models.CharField(max_length=50)

    def __str__(self):
        return self.model_name
