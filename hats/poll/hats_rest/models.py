from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)
    name = models.CharField(max_length=100)
