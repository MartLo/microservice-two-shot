import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO


def get_wardrobes():
    response = requests.get("http://wardrobe-api:8000/api/wardrobes/")
    content = json.loads(response.content)
    for wardrobe in content["wardrobes"]:
        LocationVO.objects.update_or_create(
            import_href=wardrobe["href"],
            defaults={"name": wardrobe["name"]},
        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_wardrobes()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
