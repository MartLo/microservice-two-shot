# Generated by Django 4.0.3 on 2023-07-20 18:15

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('hats_rest', '0002_locationvo_hat_fabric_hat_picture_url_hat_style_name_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='hat',
            name='fabric',
        ),
        migrations.RemoveField(
            model_name='hat',
            name='location',
        ),
        migrations.RemoveField(
            model_name='hat',
            name='picture_url',
        ),
        migrations.RemoveField(
            model_name='hat',
            name='style_name',
        ),
        migrations.DeleteModel(
            name='LocationVO',
        ),
    ]
